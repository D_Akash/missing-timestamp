import numpy as np
import pandas as pd
import datetime

dt1= pd.read_csv("data.csv")      # dataframe
dt1 = pd.to_datetime(dt1['LastUpdateTime'])     #converting datetime to format yyyy/mm/dd
dates1 = dt1         #assigning datetime data a variable
x = dt1.dt.floor('h')          #truncating datetime to hours
dates_1 = x.unique()       #getting unique of hours
List = pd.to_datetime(np.array(dates_1,dtype=np.datetime64)).tolist()    #passing to list

df01 = pd.DataFrame()          #make new dataframe
df01['datess'] = List       #passing list to newdataframe
df02 = pd.to_datetime(df01['datess'])

start_date = df02[0]         #get start-date of data 
end_date = df02[len(df02)-1]     #get end-date of data

diff = end_date - start_date        #getting number of days between date
days, seconds = diff.days, diff.seconds
hours = days * 24 + seconds // 3600      #hours between date
minutes = (seconds % 3600) // 60         #minutes between date
#seconds = seconds % 60

#creating duplicate dates
all_dates = []               
for x in range (0, (hours+1)):
    all_dates.append(start_date + datetime.timedelta(hours = x))       

df01 = pd.DataFrame({'datetime':all_dates})   #Creating dataframe
dates2 = df01['datetime'].values     #converting dataframe into array and assign a variable

#checking for missing datetime
dates_missing = []
for i in range (0,len(dates2)):            
    if (dates2[i] not in  dates_1):            #checking for datetime which are in dates2 but are not in dates1
        dates_missing.append(dates2[i])        #print missing datetime else pass
    else:
        pass      



result = pd.to_datetime(np.array(dates_missing,dtype=np.datetime64)).tolist()   #converting numpy array  to list